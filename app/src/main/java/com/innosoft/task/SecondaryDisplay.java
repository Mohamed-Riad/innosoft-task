package com.innosoft.task;

import android.app.Presentation;
import android.content.Context;
import android.os.Bundle;
import android.view.Display;

import com.asksira.loopingviewpager.LoopingViewPager;
import com.rd.PageIndicatorView;
import com.rd.animation.type.AnimationType;

import java.util.List;

public class SecondaryDisplay extends Presentation {

    private Context outerContext;

    private List<Integer> photos;
    private LoopingViewPager loopingViewPager;
    private PageIndicatorView pageIndicatorView;

    /**
     * Creates a new presentation that is attached to the specified display
     * using the default theme.
     *
     * @param outerContext The context of the application that is showing the presentation.
     * @param display      The display to which the presentation should be attached.
     */
    public SecondaryDisplay(Context outerContext, Display display, List<Integer> photos) {
        super(outerContext, display);

        this.outerContext = outerContext;
        this.photos = photos;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.secondary_display);

//        loopingViewPager = findViewById(R.id.loopingViewPager);
//        pageIndicatorView = findViewById(R.id.pageIndicatorView);
//
//        //Set IndicatorPageChangeListener on LoopingViewPager.
//        //When the methods are called, update the Indicator accordingly.
//        loopingViewPager.setIndicatorPageChangeListener(new LoopingViewPager.IndicatorPageChangeListener() {
//            @Override
//            public void onIndicatorProgress(int selectingPosition, float progress) {
//            }
//
//            @Override
//            public void onIndicatorPageChange(int newIndicatorPosition) {
//                pageIndicatorView.setSelection(newIndicatorPosition);
//            }
//        });
//
//        InfinitePagerAdapter infinitePagerAdapter = new InfinitePagerAdapter(outerContext, photos, true);
//        loopingViewPager.setAdapter(infinitePagerAdapter);
//
//        pageIndicatorView.setCount(loopingViewPager.getIndicatorCount());
//        pageIndicatorView.setAnimationType(AnimationType.SLIDE);
    }

//    @Override
//    protected void onStart() {
//        super.onStart();
//
//        loopingViewPager.resumeAutoScroll();
//    }
//
//    @Override
//    protected void onStop() {
//        super.onStop();
//
//        loopingViewPager.pauseAutoScroll();
//    }
}

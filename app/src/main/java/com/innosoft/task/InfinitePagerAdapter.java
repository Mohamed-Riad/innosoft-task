package com.innosoft.task;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.asksira.loopingviewpager.LoopingPagerAdapter;
import com.bumptech.glide.Glide;

import java.util.List;

public class InfinitePagerAdapter extends LoopingPagerAdapter<Integer> {

    public InfinitePagerAdapter(Context context, List<Integer> photos, boolean isInfinite) {
        super(context, photos, isInfinite);
    }

    //This method will be triggered if the item View has not been inflated before.
    @Override
    protected View inflateView(int viewType, ViewGroup container, int listPosition) {
        return LayoutInflater.from(context).inflate(R.layout.item_photo_pager, container, false);
    }

    //Bind your data with your item View here.
    //Below is just an example in the demo app.
    //You can assume convertView will not be null here.
    //You may also consider using a ViewHolder pattern.
    @Override
    protected void bindView(View convertView, int position, int viewType) {

        ImageView img_Photo = convertView.findViewById(R.id.img_Photo);

        Glide.with(context)
                .asBitmap()
                .load(getItem(position))
                .into(img_Photo);
    }
}

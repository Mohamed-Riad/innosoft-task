package com.innosoft.task;

import android.content.Context;
import android.content.Intent;
import android.hardware.display.DisplayManager;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.widget.Button;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SimpleDateFormat mFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", new Locale("en"));
        mFormatter.setLenient(false);

        Date dateTime = new Date();

        TextView tvDateTime = findViewById(R.id.tvDateTime);
        tvDateTime.setText(mFormatter.format(dateTime));

        List<Integer> photos = new ArrayList<>();
        photos.add(R.drawable.ic_favorite);
        photos.add(R.drawable.ic_mobile);
        photos.add(R.drawable.ic_check);

        Button btnDisplayContent = findViewById(R.id.btnDisplayContent);
        btnDisplayContent.setOnClickListener(v -> {

//            startActivity(new Intent(MainActivity.this, PresentationActivity.class));
            startActivity(new Intent(MainActivity.this, PresentationWithMediaRouterActivity.class));

//            DisplayManager displayManager = (DisplayManager) getSystemService(Context.DISPLAY_SERVICE);
//            Display[] presentationDisplays = displayManager.getDisplays(DisplayManager.DISPLAY_CATEGORY_PRESENTATION);
//            if (presentationDisplays.length > 0) {
//                for (Display presentationDisplay : presentationDisplays) {
//                    Log.d("MY_TAG", "presentationDisplayName: " + presentationDisplay.getName());
//                }
//
//                // If there is more than one suitable presentation display, then we could consider
//                // giving the user a choice.  For this example, we simply choose the first display
//                // which is the one the system recommends as the preferred presentation display.
//                Display display = presentationDisplays[0];
//                SecondaryDisplay secondaryDisplay = new SecondaryDisplay(MainActivity.this, display, photos);
//                secondaryDisplay.show();
//            }

        });
    }
}